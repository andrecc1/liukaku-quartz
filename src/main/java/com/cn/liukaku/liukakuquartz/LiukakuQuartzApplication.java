package com.cn.liukaku.liukakuquartz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class LiukakuQuartzApplication {

    public static void main(String[] args) {
        SpringApplication.run(LiukakuQuartzApplication.class, args);
    }

}
